<?php

namespace Esparksinc\Shipping\Plugin;

class ShippingMethod
{
    public function beforeAppend($subject, $result)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
        if (!$result instanceof \Magento\Quote\Model\Quote\Address\RateResult\Method) {
            return [$result];
        }

        // if ($this->isMethodRestricted($result)) {
            // $result->setIsDisabled(true);

        $logger->debug(print_r([$result]));
        // }

        return [$result];
    }

    public function isMethodRestricted($shippingModel)
    {
        // $code = $shippingModel->getCarrier();
        // $restrictedMethod = ['flatrate_flatrate'];

        // if ($restrictedMethod && in_array($code, $restrictedMethod)) {
        //     return true;
        // }

        return false;
    }
}