<?php

// namespace Esparksinc\Shipping\Plugin;

// class DisableFreeShipping
// {
//     /**
//      * @param \Magento\OfflineShipping\Model\Carrier\Freeshipping $subject
//      * @param callable $proceed
//      * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
//      * @return \Magento\Shipping\Model\Rate\Result|bool
//      */
//     public function afterCollectRates(
//         \Magento\OfflineShipping\Model\Carrier\Freeshipping $subject,
//         $result,
//         \Magento\Quote\Model\Quote\Address\RateRequest $request
//     ) {
//         if ($request->getDestPostcode()) { // Check is postcode exists in request
//             if ($this->postCodeContainsNullOnSecondPosition($request->getDestPostcode())) { // Check is second symbol == 0
//                 return false; // Disable method
//             }
//         }

//         if ($request->getDestRegionId()) { // Check is regionid exists in request
//             $regionid = $request->getDestRegionId() ;
//             if ($regionid == 69 || $regionid == 72 || $regionid == 73 || $regionid == 75 || $regionid == 78  ) { 
//                 return false; // Disable method
//             }
//         }

//         return $result;
//     }

//     /**
//      * Test postcode
//      *
//      * @param string $postCode
//      * @return bool
//      */
//     private function postCodeContainsNullOnSecondPosition(string $postCode): bool
//     {
//         return stripos($postCode, '0') === 1;
//     }
}