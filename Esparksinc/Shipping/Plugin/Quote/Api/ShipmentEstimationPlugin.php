<?php

namespace Esparksinc\Shipping\Plugin\Quote\Api;

use Magento\Quote\Api\ShipmentEstimationInterface;

class ShipmentEstimationPlugin
{
    public function __construct(
        \Magento\Customer\Model\Session $customerSession
       
    ) {
        $this->customerSession = $customerSession;

    }

    public function aroundEstimateByExtendedAddress(
        ShipmentEstimationInterface $subject,
        \Closure $proceed,
        $cartId,
        \Magento\Quote\Api\Data\AddressInterface $address
    ) {

  
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/testshipping.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
       
       
       $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
       $cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
       $itemsCollection = $cart->getQuote()->getItemsCollection();
       $itemsVisible = $cart->getQuote()->getAllVisibleItems();
       $items = $cart->getQuote()->getAllItems();
       $product = $objectManager->create('Magento\Catalog\Model\Product');

       $brands =  array();
       foreach($items as $item) {
        $loaded_product = $product->load($item->getProductId());
        $product = $product->load($item->getProductId());
        $brand = null;
        $brand=$loaded_product->getResource()->getAttribute('brand')->getFrontend()->getValue($loaded_product);
        $brand = strval($brand);
        $subTotal = $cart->getQuote()->getSubtotal();
        // $items = array($brand);
        array_push($brands, $brand);

        }

        
        $size = sizeof($brands);
        $logger->info($brands);
        $logger->info($size);
        $pelican = in_array("Pelican",$brands);
        $nanuk = in_array("Nanuk", $brands);
        $other = in_array("Other", $brands);


        $logger->info("Pelican");
        $logger->info($pelican);
        $logger->info("Nanuk");
        $logger->info($nanuk);
        $logger->info("Other");
        $logger->info($other);
       

        $code=$address->getPostcode();
        $region=$address->getRegionId();
        $logger->info($code);
        $logger->info($region);

	    $check = stripos($code, '0') === 1;
		$logger->info(stripos($code, '0') === 1);

        

        $shippingMethods = $proceed($cartId, $address);
        
        if ($code) { 
            if ($check ) { 
                foreach ($shippingMethods as $key => $shippingMethod) {
                    if($shippingMethod->getMethodCode() == "freeshipping"){
                        unset($shippingMethods[$key]);
                    }        
                }
            }
        }

        if ($region) { // Check is regionid exists in request
            $regionid = $region;
            if ($regionid == 69 || $regionid == 72 || $regionid == 73 || $regionid == 75 || $regionid == 78) { 
                foreach ($shippingMethods as $key => $shippingMethod) {
                    if($shippingMethod->getMethodCode() == "freeshipping"){
                        unset($shippingMethods[$key]);
                    }        
                }
            }
        }
          
        if( in_array("Pelican",$brands) || in_array("Nanuk",$brands) || in_array("Pelican-Hardigg",$brands) || in_array("Pelican-Storm",$brands) ) {
            if($subTotal >= 150){    
                if( in_array("Other", $brands) || in_array("SKB", $brands)) 
                {            
            	 if ($regionid == 69 || $regionid == 72 || $regionid == 73 || $regionid == 75 || $regionid == 78 || $check) {		                    																				
                    foreach ($shippingMethods as $key => $shippingMethod) {
                    		if($shippingMethod->getMethodCode() == "freeshipping"){
                      		  unset($shippingMethods[$key]);
                    		}        
            			}
            			$logger->info("Pelican & Non Pelican found");
            			$logger->info("You are not eligible for free shipping");
            			
                	}
                	else
                	{
                                   foreach ($shippingMethods as $key => $shippingMethod) {
                            // if($shippingMethod->getMethodCode() == "freeshipping"){
                                unset($shippingMethods[$key]);
                            // }        
                            }
                        $logger->info("Pelican & Non Pelican found");
                        $logger->info("You may be eligible for free shipping");


                    // return '<h1>'. $result . 'Mageplaza.com' .'</h1>';
                    // $message = "You may be eligible for free shipping. Contact at websales@productioncase.com for you free quote";
                    // return $message;
                    // return '<h1> Mageplaza.com </h1>';
                   	// $question = $objectManager->create('\Magento\Framework\Message\ManagerInterface');
                   	// $question->addWarning(__("Warning"));
           
                	}
                    
                }
                else
                {
                    $logger->info("Only Pelican/Nanuk found");
                }            
            }else{
                foreach ($shippingMethods as $key => $shippingMethod) {
                    if($shippingMethod->getMethodCode() == "freeshipping"){
                        unset($shippingMethods[$key]);
                    }        
                }
            }
        }
        else {   
            foreach ($shippingMethods as $key => $shippingMethod) {
                if($shippingMethod->getMethodCode() == "freeshipping"){
                    unset($shippingMethods[$key]);
                }        
            }
        }
            
    return $shippingMethods;

	}

}